\section{Introduction}
\label{sec:introduction}

The exclusive $b\to s\gamma $ process is one of the most sensitive observables to new physics in $B$ physics: unlike many other $b$-hadron decays, it is described by a single operator, the electro-magnetic type $\overline{s}\sigma_{\mu\nu}(1+\gamma_5)bF^{\mu\nu}$,  which minimizes the uncertainties from hadronic effects. 
In the era of the LHC and the upgraded $B$ factory experiment, Belle II, an interesting opportunity opens to investigate the circular-polarization of the photon in $b\to s\gamma$ process and gain additional insight on its nature. 

In the standard model (SM), the photon polarization of $b\to s\gamma$ is predicted to be predominantly left-handed due to the operator mentioned above.
On the other hand, several new-physics  models contain new particles that couple differently from the SM, namely, inducing an opposite chirality operator $\overline{s}\sigma_{\mu\nu}(1-\gamma_5)bF^{\mu\nu}$; these models predict an enhanced right-handed photon contribution. Examples of such new physics models are given in Refs.~\cite{Becirevic:2012dx,Kou:2013gna,Haba:2015gwa,Paul:2016urs}. 
The photon polarization in $b\to s\gamma$ transitions is therefore a fundamental property of the SM, and its determination may provide information on physics beyond the SM.

The experimental measurement of the photon polarization has been a real challenge in $B$ physics, and much effort has been put into it in recent years.
Mainly, two ways to measure the photon polarization have been proposed and carried out: measuring the angular distribution of the recoiled particles (see~\cite{Gronau:2001ng,Gronau:2002rz,Kou:2010kn, Kou:2016iau,Bishara:2015yta,Oliver:2010im} for theoretical proposals and~\cite{Aaij:2015dea,Aaij:2014wgo} for experimental results), and measuring the time-dependent \CP asymmetry (Refs.~\cite{Atwood:1997zr,Atwood:2004jj,Atwood:2007qh, Muheim:2008vu} and~\cite{Aubert:2008gy,Ushiroda:2006fi, Li:2008qma,Sanchez:2015pxu} for theory and experiment, respectively). 
In this article, we discuss the second method.

Extracting information on the photon polarization from the time-dependent \CP asymmetry measurement is illustrated with the promising channel $B\to K_{\rm res}\gamma \to \KS (n\pi)\gamma$, where $K_{\rm res}$ is a kaonic resonance and $n\pi$ designates $n$ pions that form a \CP eigenstate. The illustration is depicted in Fig~\ref{fig:1}.
%The way to extract information on the photon polarization from the  the time-dependent \CP asymmetry  measurement is schematically depicted in Fig.~\ref{fig:1}, using the promising channel $B\to K_{\rm res}\gamma \to \KS (n\pi)\gamma$, where $K_{\rm res}$ is a kaonic resonance and $n\pi$ designates $n$ pions that form a \CP eigenstate. 
The time-dependent \CP asymmetry originates from the interference of the processes $B\to K_{\rm res}\gamma \to \KS (n\pi)\gamma$ and  $\Bb \to \overline{K}_{\rm res}\gamma \to \KS (n\pi)\gamma$, one of which appears from a $B-\Bb$ oscillation. The important point is that interfere occurs only when photons coming from $B$ and $\Bb$ are circularly polarized in the same direction. 
Let us define the rate of  $\Bb$ ($B$) mesons decaying into left- (right-) handed photons to be $c$, and the rate of $\Bb$ ($B$) into right- (left-) handed photons  to be $c^{\prime}$. The former process is induced by the standard operator contribution $\overline{s}\sigma_{\mu\nu}(1+\gamma_5)bF^{\mu\nu}$, and the latter is induced by the non-standard operator $\overline{s}\sigma_{\mu\nu}(1-\gamma_5)bF^{\mu\nu}$.
%Let us define the rate of  $\Bb$ ($B$) mesons decaying into left- (right-) handed photons (induced by the standard operator contribution $\overline{s}\sigma_{\mu\nu}(1+\gamma_5)bF^{\mu\nu}$) to be $c$, and the rate of $\Bb$ ($B$) into right- (left-) handed photons (induced by the non-standard operator $\overline{s}\sigma_{\mu\nu}(1-\gamma_5)bF^{\mu\nu}$) to be $c^{\prime}$. 
Knowing that in the SM $c^{\prime}/c=m_s/m_b (\simeq 0)$, that is, the left- (right-) handed photon is nearly forbidden for a $B$ $(\Bb)$ meson decay, the interference of $B$ and $\Bb$ would
%not occur.
\eli{be nearly zero.} Therefore, an observation of the non-zero \CP asymmetry can be considered to be the immediate signal of new physics. Once non-zero \CP asymmetry is observed, one can further determine the ``photon polarization'', which means to determine the ratio of $c^{\prime}/c$ by using the oscillation parameters ($q/p$ in Fig.~\ref{fig:1}) obtained from the $\sin 2\beta$ measurement in other modes, such as $B\to J/\psi \KS$.

The simplest channel to study in this regard is  $\Bz\to \Kstarz\gamma\to \KS\pi^0\gamma$, for which the first
measurements of the mixing induced \CP violation have been
obtained by the \babar~\cite{Aubert:2008gy} and Belle~\cite{Ushiroda:2006fi} experiments:
$S_{\KS\pi^0 \gamma}^{\scriptsize \babar}=-0.03\pm 0.29\pm 0.03, \ \ 
S_{\KS\pi^0 \gamma}^{\rm Belle}=-0.32^{+0.36}_{-0.33}\pm 0.05$. As the statistical uncertainties dominate in these measurements,
they could
be largely improved by the Belle II experiment, where it is planed
to accumulate in the coming ten years 70 to 100 times more data than in the first-generation $b$ factories.


In this paper, we discuss an alternative method that can
 provide a measurement of the mixing induced \CP violation in the decay $\Bz\to K_{\rm res}\gamma \to \rhoz \KS \gamma \to \pi^+\pi^-\KS\gamma$. 
Investigating this channel allows
firstly to confirm the other measurements, and secondly to increase the experimental sensitivity, especially in the early time of the Belle II experiment. 

The main difficulty comes from the fact that the final state $\KS\pi^+\pi^-$ can originate not only from the \CP eigenstate $\rhoz \KS$ but also from other intermediate states. 
The determination of the \CP asymmetry of the $\rhoz \KS$ channel is disturbed in particular by non \CP eigenstates, such as $K^{*\pm}\pi^\mp$.
%In particular, non \CP eigenstates, such as $K^{*\pm}\pi^\mp$, disturb the determination of the \CP asymmetry of the $\rhoz \KS$ channel.
In order to disentangle these contributions,
a detailed amplitude analysis is required. Such analysis
 has been pioneered by the Belle collaboration~\cite{Li:2008qma} and extended by the \babar collaboration~\cite{Akar:2013ima}.

\begin{figure}[htb]
%\hspace*{-0.8cm}\begin{minipage}{8.8cm}
%\hspace*{-0.6cm}\begin{minipage}{8.5cm}
{\relsize{-1.3}
\begin{minipage}{0.48\textwidth}
\begin{eqnarray}
 & \Bz \ {{c^{\prime}(c)}  \atop \longrightarrow}\ K_{\rm res} \gamma_{L (R)}  & \nonumber \\
\phantom{B(0)}^{ f_+(t)}\nearrow& \hspace*{2.cm}& \searrow \nonumber \\
\Bz(t=0)\hspace*{0.5cm}&\hspace*{2.cm}&  (n\pi) \KS \gamma_{L (R)} \; \nonumber \\
\phantom{B(0)}_{ \frac{q}{p}f_-(t)}\searrow & \hspace*{2.cm}& \nearrow\nonumber \\
& \Bzb \ {{c(c^{\prime})}  \atop \longrightarrow}\ \overline{K}_{\rm res} \gamma_{L (R)} & \nonumber \end{eqnarray}
\end{minipage}
%%%
%\begin{minipage}{8.5cm}
\hspace*{0.3cm}\begin{minipage}{0.48\textwidth}
\begin{eqnarray}
 & \Bz\ {{c^{\prime}(c)}  \atop \longrightarrow}\ K_{\rm res} \gamma_{L (R)}  & \nonumber \\
\phantom{B(0)}^{\frac{p}{q}f_-(t)}\nearrow& \hspace*{2.cm}& \searrow\nonumber \\
\Bzb(t=0)\hspace*{0.5cm}&\hspace*{2.cm}&   (n\pi) \KS \gamma_{L (R)} 
\nonumber \\ 
\phantom{B(0)}_{ f_+(t) }\searrow & \hspace*{2.cm}& \nearrow \nonumber \\
& \Bzb \ {{c(c^{\prime})}  \atop \longrightarrow}\ \overline{K}_{\rm res} \gamma_{L (R)} & \nonumber 
\end{eqnarray}
\end{minipage}
}
\caption{Schematic description of the time-dependent \CP asymmetry of $B\to K_{\rm res} \gamma \to n\pi  \KS \gamma$.
\eli{The factors $f_-(t)$ and $f_+(t)$ are the time-dependent oscillation and non-oscillation rates, respectively, of a $\Bz$ or a $\Bzb$ meson.}
The $q$ and $p$ are the $B-\Bb$ oscillation parameters, which correspond to  $q/p=\frac{V_{tb}^*V_{td}}{V_{tb}V_{td}^*}=e^{-2i\beta}$ in the SM.
The coefficients $c$ and $c^{\prime}$ represent the ratio of the the standard operator contribution $\overline{s}\sigma_{\mu\nu}(1+\gamma_5)bF^{\mu\nu}$ and that of the non-standard one $\overline{s}\sigma_{\mu\nu}(1-\gamma_5)bF^{\mu\nu}$, respectively. 
In the SM, $c^{\prime}/c=m_s/m_b (\simeq 0)$, and therefore one of the passes is forbidden, the interference would not occur, and the CP asymmetry would be zero.}
\label{fig:1}
 \end{figure}


In this paper, motivated by these developments, we re-visit the method to obtain the mixing induced \CP asymmetry for $\Bz\to \rhoz \KS \gamma$ in $\Bz\to K_{\rm res}\gamma \to \pi^+\pi^-\KS\gamma$ decays
% that provides further
\eli {to gain more}
insight on the photon polarization.
One indispensable ingredient of the method is the \CP-eigenvalue, or ``\CP-sign'', that intervenes in the measurement, considering the dominant intermediate decay channels: 
\begin{eqnarray}
K_1(1270), \quad K_1(1400) && (J^P=1^+), \nonumber \\
K^*(1410), \quad K^*(1680) && (J^P=1^-), \nonumber \\
K_2^*(1430) && (J^P=2^+), \nonumber 
\end{eqnarray}
for the kaonic resonances and 
 \begin{eqnarray}
&B\to \Kres \gamma \to (\rho \KS) \gamma \to \KS(\pi^+\pi^-)\gamma, \nonumber \\
& B\to \Kres \gamma \to (K^{*+} \pi^-) \gamma \to (\KS\pi^+)\pi^-\gamma,& \nonumber \\
& B\to \Kres \gamma \to (\swave^{+} \pi^-) \gamma \to (\KS\pi^+)\pi^-\gamma, &\nonumber 
\end{eqnarray}
for the $K\pi$ or $\pi\pi$ intermediate states. The notation $\swave$ designates the $K\pi$ s-wave.
A derivation of this \CP-sign has not been demonstrated before.
Another ingredient of the method is the ``dilution factor'' that allows to obtain the specific value of the mixing induced \CP asymmetry for $\Bz\to \rhoz \KS \gamma$ from the raw measurement of this quantity in $\Bz\to \pi^+\pi^-\KS\gamma$ decays.



In \secref{sec:overview} we introduce the time dependent \CP asymmetry formulae for $B\to \Kres \gamma \to \KS\pi^+\pi^-\gamma$ decays and the dilution factor. 
In \secref{sec:amplitudes} we derive the \CP-sign for the decay amplitudes with different intermediate states, \eli{which is required in order to extract the \CP asymmetry}.
Using this result we derive the dilution factor in \secref{sec:dilutionFactor},
and in \secref{sec:application} we describe the way to extract it from $\Bp \to \Kresp \gamma \to K^+ \pi^-\pi^+\gamma$ decays.\footnote{Charge conjugation is implicit throughout the document.}
Finally, in \secref{sec:interpretation} we discuss the future prospects for the measurement of the \CP asymmetry at Belle II and for that of the dilution factor at LHCb. We then conclude in \secref{sec:conclusion}.















