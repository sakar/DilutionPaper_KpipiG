%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{\boldmath{Time dependent \CP asymmetry for $\Bz \to \Kres \g \to \pip \pim \KS \g$ and the dilution factor \D}}\label{sec:overview}
We define the time dependent \CP asymmetry for \nChannelRhoz decays as 
\begin{equation}
\frac{\overline{\Gamma}_{\rhoKs \g}(t)-\Gamma_{\rhoKs \g}(t)}{\overline{\Gamma}_{\rhoKs \g}(t)+\Gamma_{\rhoKs \g}(t)}\equiv 
\SKsrho \sin(\dm t) - \CKsrho \cos(\dm t)
\label{eq:2-1_v2}
\end{equation}
with
\begin{equation}
\SKsrho = \frac{ 2\Imag{\qoverp \sumLR{\AmpTotRhoKsConj \AmpTotBarRhoKs}}}{\sumLR{ \ModSq{\AmpTotBarRhoKs} + \ModSq{\AmpTotRhoKs}}}, \quad 
\CKsrho=-\frac{\sumLR{ \ModSq{\AmpTotBarRhoKs} - \ModSq{\AmpTotRhoKs}}}{\sumLR{ \ModSq{\AmpTotBarRhoKs} + \ModSq{\AmpTotRhoKs}}},
\label{eq:v3-2_v2}
\end{equation}
where \AmpTotRhoKs and \AmpTotBarRhoKs correspond to the \Bz and \Bzb decay amplitudes, respectively, with the left- and right-handed photon polarisation, designated by $\lambda=L, R$.
Integration over the phase space is implicit. 
In this article we adopt the convention\footnote{This convention is equivalent to
\mbox{$\C \ket{\Bz} = - \ket{\Bzb}$}.}
\mbox{$\CP \ket{\Bz} = + \ket{\Bzb}$}. 
The \CP even/odd states are defined as \mbox{$\ket{B_{1/2}} = p\ket{\Bz} \pm q\ket{\Bzb}$} with 
\begin{equation}
\qoverp=+\sqrt{\frac{{\rm M}_{12}^*-\frac{i}{2}\Gamma^*_{12}}{{\rm M}_{12}-\frac{i}{2}\Gamma_{12}}},	
\end{equation}
and the mass difference is taken such that 
\begin{equation}
\dm={\rm M}_2-{\rm M}_1 = -2\Real{\qoverp({\rm M}_{12}-i\frac{\Gamma_{12}}{2})}.	
\end{equation}
%where $\AmpTotRhoKs$ and $\AmpTotBarRhoKs$ are, respectively, \B and \Bb decay amplitudes with photon polarisation $\lambda=L, R$. 
In the SM, $q/p=(\Vtbstar\Vtd)/(\Vtb\Vtdstar)=e^{-2i\beta}$.
In the rest of the article the notation used for the weak \Bz-\Bzb mixing phase is $\beta$ rather than its equivalent, $\phi_1$.

Due to the small strong phase that can be generated in this decay channel, the direct \CP violation is expected to be negligible.
It appears from the expression of \SKsrho in \Eqref{eq:v3-2_v2} that unless the \Bz and \Bzb can both decay into final states with the same photon polarisation $\lambda$, the numerator becomes zero, and no mixing induced \CP violation is expected. 
On the other hand, if the interference is non-zero, mixing induced \CP violation may have observable effects, and thus its size needs to be determined, which requires the knowledge of the \CP signs inherited by the decay of the kaonic resonances. 
These are derived in \secref{sec:amplitudes}. 

As discussed in \secref{sec:introduction}, other intermediate states than \rhoKs are expected in \mbox{$\Bz \to \Kres \g \to \pip \pim \KS \g$} decays, and these need to be carefully separated.
Including all contributions, the time dependent \CP asymmetry expression becomes 
\begin{equation}
\frac{\overline{\Gamma}_{\pip \pim \KS \g}(t) - \Gamma_{\pip \pim \KS \g}(t)}{\overline{\Gamma}_{\pip \pim \KS \g}(t) + \Gamma_{\pip \pim \KS \g}(t)} \equiv 
\SKspipi \sin(\dm t) - \CKspipi \cos(\dm t)
\label{eq:2-1}
\end{equation}
with 
\begin{equation}
	\SKspipi =  \frac{2\Imag{\qoverp \sumLR{ \AmpTotConj \AmpTotBar} }}{\sumLR{ \ModSq{\AmpTotBar} + \ModSq{\AmpTot} }}, \quad 
	\CKspipi = -\frac{\sumLR{ \ModSq{\AmpTotBar} - \ModSq{\AmpTot} }}{\sumLR{ \ModSq{\AmpTotBar} + \ModSq{\AmpTot} }},
\label{eq:v3-2}
\end{equation}
where the integration over the phase space is implicit. The \Bz  and \Bzb decay amplitudes, \AmpTot and \AmpTotBar, respectively, are now sums over the considered intermediate states: 
\begin{eqnarray}
	\AmpTot    &=& \AmpTotRhoKs    + \AmpTotKstPi    + \AmpTotKappaPi    \label{eqv1:1},\\
	\AmpTotBar &=& \AmpTotBarRhoKs + \AmpTotBarKstPi + \AmpTotBarKappaPi \label{eqv1:2}.
\end{eqnarray}

Due to the limited size of the available data-samples, a time-dependent amplitude analysis allowing to disentangle the different amplitudes and thus to measure  the \CP parameters for individual intermediate states is not feasible.
Alternatively, the mixing-induced \CP violation parameter for the whole $K \pi \pi \g$ system, \SKspipi, can be obtained after integrating over the different intermediate states.
The mixing induced \CP asymmetry for \nChannelRhoz decays can then be obtained from the relation between the measured value of \SKspipi and the so-called dilution factor \D:
\begin{eqnarray} 
\label{eq:v3-9}
\D&=&\frac{\SKspipi}{\SKsrho}  \\
\label{eq:v3-10}
&=& \frac{\sumLR{ \ModSq{\AmpTotRhoKs} + \ModSq{\AmpTotBarRhoKs} }}{\sumLR{ \ModSq{\AmpTot} + \ModSq{\AmpTotBar} }}
	\frac{\Imag{\qoverp \sumLR{ \AmpTotConj \AmpTotBar} }}{\Imag{\qoverp \sumLR{ \AmpTotRhoKsConj \AmpTotBarRhoKs} }}, 
\end{eqnarray}
where, once again, the integration over the phase space is implicit.

In order to get the best sensitivity on \D, its value can be measured from an amplitude analysis of $\Bp \to \Kresp \g \to \Kp \pim \pip \g$ decays, assuming the conservation of Isospin. 
Indeed, due to a larger branching fraction as well as higher experimental reconstruction and selection efficiencies, a larger data sample is expected for the $\Kp \pim \pip \g$ final state compared to the neutral mode. 

%In either way whether we separate  the different intermediate states or not, the extraction of the photon polarisation information from this measurement requires the so-called \CP sign for those intermediate states, which we derive in the next section.

%As shown in \secref{sec:amplitudes}, our discussion does not depend on the spin-parity of the kaonic state. 
%Thus, the formulae derived in this article can be applied either for individual kaonic states with given $J^P$ or for their sum.




