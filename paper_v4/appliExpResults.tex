%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{\boldmath{Experimental extraction of \SKsrho}}
\label{sec:application}

Time-dependent Dalitz-plot analysis to extract the \CP asymmetry for individual resonances is currently not feasible at \B factories due to the limited size of the available samples. Indeed, only the \CP asymmetry of the full $K\pi\pi$ system is measured in~\cite{Li:2008qma, Sanchez:2015pxu}. Then, the \CP asymmetry of the \rhoKs channel, which can be directly related to the photon polarisation as shown in the previous section, is obtained via the factor\\
\hspace*{-2cm}
\begin{eqnarray}
\D & \equiv & \frac{\SKspipi}{\SKsrho} \label{seq:v2_28}\\
 		& = & \frac{\sumLR{ \ModSq{\AmpTotRhoKs} - \left( \Real{\AmpTotBarKstPiConj \AmpTotKstPi}
             -2\Real{\AmpTotRhoKsConj \AmpTotKstPi} +\Kstpm \leftrightarrow \swavepm \right) }}
            	{\sumLR{ \ModSq{\AmpTotRhoKs} + \left(\ModSq{\AmpTotKstPiConj}
             +2\Real{\AmpTotRhoKsConj \AmpTotKstPi} + \Kstpm \leftrightarrow \swavepm \right) }} \nonumber \\
 		& = & \frac{\sumLR{ 1 - \left( \frac{\Real{\AmpTotBarKstPiConj \AmpTotKstPi}}{\ModSq{\AmpTotRhoKs}}
             -2\frac{\Real{\AmpTotRhoKsConj \AmpTotKstPi}}{\ModSq{\AmpTotRhoKs}} +\Kstpm \leftrightarrow \swavepm \right) }}
            	{\sumLR{ 1 + \left(\frac{\ModSq{\AmpTotKstPiConj}}{\ModSq{\AmpTotRhoKs}}
             +2\frac{\Real{\AmpTotRhoKsConj \AmpTotKstPi}}{\ModSq{\AmpTotRhoKs}} + \Kstpm \leftrightarrow \swavepm \right) }}, \nonumber 
\end{eqnarray}
that can be  extracted by a time-integrated analysis.
It is clear from this expression that optimised vetoes to reduce the $\Kst\pi$ and $\swavepm$ contributions could maximise the sensitivity on \SKsrho.
\emi{In Ref.~\cite{Li:2008qma}, it is further proposed to use the isospin relation and to extract the amplitudes of resonant modes needed to compute this dilution factor  via 
the \cChannelpm modes}, which have more signal events. In this section, we explain the procedure more in detail namely following the analysis from \babar in Ref.~\cite{Sanchez:2015pxu}.
The parameterisation used therein is recalled in Appendix~\ref{App:BabarParam}.
\minor{In this analysis, a region of the phase space where the $\rhoz\KS$ component is enhanced while the proportions of the $\Kstarp\pim$ and the \KappaPi S-wave components are suppressed are considered such that the measured time-dependent \CP asymmetry is close to \SKsrho (i.e. absolute value of dilution factor is close to one)}. %which can be directly related to the photon polarisation parameters through \Eqref{eq:sv2_28}}. 
This is achieved by selecting a region in the \mKpi--\mpipi plane corresponding to $600 \leq \mpipi \leq 900\mevcc$ and $\mKpi^{\rm min} \leq \mKpi \leq 845\mevcc$ or $945\mevcc \leq \mKpi \leq \mKpi^{\rm max}$, where $\mKpi^{\rm min}$ and $\mKpi^{\rm max}$ denote the allowed phase-space boundaries in the \mKpi dimension. 

As mentioned at Sec.~\ref{sec:dilutionFactor}, the expression of the dilution factor is valid across the whole $K\pi\pi$ phase space and can be integrated. Thus, its value changes in different regions of phase space, ‏e.g., $K_1(1270)$ has a larger BF to $\rho$ than higher-spin resonances. A cut on \mKpipi can be considered in order to optimise the measurement of the dilution factor. \simon{Add a reference to PDG, check if everything we are saying here is exact and consider to stress this by values, e.g. from the PDG, for the different kaonic resonances. Stress that some additional measurements of branching fractions could be beneficial.}

The point is that the extraction of the dilution factor \D, which does not require a \CP asymmetry measurement but only a study of the intermediate resonance amplitudes, can be obtained from the other measurements, performed on a much larger data sample, for instance with the $\Bp \to \Kp \pip \pim \g$ channel in the LHCb experiment.


\begin{comment}
Using this selection, the dilution factor was found to be:
\begin{equation}
\D = -0.78^{+ 0.19}_{-0.17}.
\end{equation}

The fact that \D is negative is not surprising, given that in Ref.~\cite{Sanchez:2015pxu}, both interference terms: between the components $\rhoz \Kp$ and $\Kstarz \pip$, as well as between $\rhoz \Kp$ and $\swave \pip$, exhibit significantly destructive patterns. 

In Ref.~\cite{Li:2008qma}, the $\swave \pip$ component was not included, and thus the interference between the components $\rhoz \Kp$ and $\Kstarz \pip$ was found to be constructive.
This constitutes one of the main difference between the results from \babar\ and  Belle.
%which remains to be understood.
\end{comment}

\begin{comment}
Recently, the Babar collaboration reported the measurement of the dilution factor in the $B^{0}\to K_{S}^{0}\rho^{0}\g$ decay, [1512.03579]
\begin{equation}\label{DilutionFactorBabar}
\D_{K_{S}^{0}\rho^{0}\g}^{\rm Babar}=-0.78^{+0.19}_{-0.17},
\end{equation}
using the formalism of Eq.(26) {\color{red}(in the note) },
which leads to the time-dependent $CP$ asymmetry
\begin{equation}\label{SKsrhogammaBabar}
\mathcal{S}_{K_{S}^{0}\rho^{0}\g}=-0.18\pm0.32^{+0.06}_{-0.05},
\end{equation}
by the measurement of $\mathcal{S}_{K_{S}^{0}\pip\pim\g}=0.14\pm0.25\pm0.03$. 
The measured dilution factor in \Eqref{DilutionFactorBabar} is quite different from the previous Belle result [PRL101,251601(2008)], $\D_{K_{S}^{0}\rho^{0}\g}^{\rm Belle}=0.83^{+0.19}_{-0.03}$, even with a minus sign. Two reasons result in such difference between the Belle and Babar results: i) the formula used by Babar has a minus sign for $K^{*}\pi$ component, compared to the one by Belle; ii) the significant contributions of $S$-wave $K\pi$ resonances, as stressed in this paper, have been considered by Babar, but not by Belle. 
\end{comment}