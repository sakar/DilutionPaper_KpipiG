%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Interpretation of the results and future prospects}\label{sec:interpretation}

\simon{Check if this section contains references to- and comparison with- other methods providing information about the photon polarisation, e.g., $A_{ud}$.}



Our goal is to measure the \SKsrho which is directly related to the polarisation parameter via \Eqref{eq:sv2_28}. We can obtain \SKsrho from the measurements of \SKspipi and \D by %\footnote{As the \CP violation $\SKspipi$ is bounded by -1 to 1, this formula is not exact. Modification should be introduced near the boundary. } 
\begin{align}
\SKsrho=\frac{\SKspipi}\D,
\end{align}
\begin{align}
\label{sigmaSKsrho}
\sigma_{\SKsrho}\simeq\frac{\SKspipi}\D\sqrt{\left(\frac{\sigma_{\SKspipi}}{\SKspipi}\right)^2+\left(\frac{\sigma_\D}\D\right)^2}.
\end{align}

Then, the statistical significance of \SKsrho being different from a hypothetical point $(\SKsrho)_0$ is 
\begin{equation}
\frac{\left|\SKsrho-(\SKsrho)_0\right|}{\sigma_{\SKsrho}}=\frac{\left|\SKspipi -(\SKsrho)_0\D\right|}{\sqrt{\sigma_\SKspipi^2+(\SKsrho)_0^2\sigma_{\D^2}}}.
\label{eq:sv242}
\end{equation}
We should first notice that if we assume that the SM is fully left-handed, i.e. $(\SKsrho)_0=0$,  the \textit{SM null test} can be performed only with the \SKspipi measurement since $\frac{\left|\SKsrho\right|}{\sigma_{\SKsrho}}=\frac{\left|\SKspipi \right|}{\sigma_\SKspipi}$. Thus, a significant deviation of the measured $\SKspipi$ from zero can be directly interpreted as a discovery of new physics irrespective to the dilution factor. For example, the current central values $\SKspipi\simeq 0.1-0.2$, if confirmed, would lead to 5-10 $\sigma$ effect of new physics with the foreseen Belle II sensitivity,  $\sigma_\SKspipi \sim 0.02-0.03$.  

On the other hand, for a precise measurement of  \SKsrho, we need the measurement of the dilution factor. We should also note that $\SKspipi\simeq 0$ can not immediately exclude  $\SKsrho\neq 0$ unless $\D\neq 0$ is confirmed. 

The dilution factor \D can be obtained independently from the time-dependent \CP asymmetry as described in \secref{sec:application}. In particular, since we can use the fully charged final states, $\Bp\to K^{+}\pip\pim\g$, the LHCb experiment which has much larger data on this decay channel can provide the most precise measurement of the dilution factor.  
In~\cite{Aaij:2014wgo}, with the integrated luminosity of 3\fb by LHCb, the number of collected events of \cChannelpm is about five times  more than the current results by the Belle or Babar. 

To be more precise, the \babar analysis~\cite{Sanchez:2015pxu} reconstructed $\sim 2.5$k  \cChannelpm  events while LHCb did in~\cite{Aaij:2014wgo} about $14$k events with 3\fb of data. This means that the uncertainty in the dilution factor can be reduced immediately by a factor of 2 to 3 and by the end of Run II (8\fb of data is expected), we can reduce another factor of 3. 

On the other hand, for the measurement of the time-dependent \CP asymmetry of \nChannelPiPi, we need to wait for the Belle II experiment. 
In Ref.~\cite{Sanchez:2015pxu}, the main systematic uncertainty on \SKspipi is due to the knowledge of the probability density functions in the maximum likelihood fit. This is related to the amount of simulated data, which can be assumed to roughly scale with the integrated luminosity of an experiment. 
More specifically, the expected statistical accuracy would be reduced according to
\begin{align}
\sigma_{\rm Belle\ \!\!II} = \sqrt{(\sigma_{\rm stat}^{2}+\sigma_{\rm sys}^{2}){\mathcal{L}_{\rm Belle}\over\mathcal{L}_{\rm Belle\ \!\!II}}+\sigma_{\rm irred}^{2}},
\end{align} 
where the irreducible systematic uncertainty is 
% is the systematic uncertainty e.g. from $K^{0}/\overline K^{0}$.
typically $\sigma_{\rm ired}\approx0.02\%$. 
Accordingly, we evaluate the naive future prospect in Table~\ref{tab:extrapolations}, which shows that the statistical sensitivity on \SKspipi at the Belle II experiment is of order of $0.02-0.03$. 

We can conclude that within a few years, the measurement of \SKsrho will be drastically improved and this channel will play a very strong supporting role with respect to the golden channel for the photon polarisation measurement via \CP violation, such as using the $\Bz \to \KS \piz \g$ channel.   

\vspace*{2cm}
\begin{table}[!hptb]
\begin{center}
\begin{tabular}{ccccc}
\hline\hline
 & \babar~\cite{Sanchez:2015pxu} & Belle II (10\ab) & Belle II (50\ab) & LHCb (8\fb)
 \\
 \hline
 $\int \! \mathcal{L}$ & $\sim 500\fb$  &  10\ab & 50\ab & 8\fb
 \\
\SKspipi &   $0.14 \pm 0.25 \pm 0.03$ & ${}\pm 0.07$ & ${}\pm 0.03$ & $-$
\\
\D & $-0.78^{+0.19}_{-0.17}$ & ${}\pm 0.05$ & ${}\pm 0.02$
 & ${}\pm 0.05$\\
\SKsrho & $-0.18 \pm 0.32^{+0.06}_{-0.05}$ & ${}\pm 0.09$ & ${}\pm 0.04$ & $ - $
\\
\hline
\end{tabular}
\end{center}
\label{tab:extrapolations}
\caption{Future prospects of the \SKspipi, \D, \SKsrho measurements. The errors of the future Belle II measurement are extrapolated from the \babar result in~\cite{Sanchez:2015pxu} assuming that the statistical errors are dominant. LHCb Run II prospect is obtained by extrapolating the Run I result in~\cite{Aaij:2014wgo}. }
\end{table}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{comment}
The formalism developed in the present paper is related to the current experimental status of the measurement of the time-dependent \CP violation parameter \SKsrho. 
Indeed, given the limited size of the available data samples, it was hardly possible to avoid the use of the dilution factor in the existing time-dependent analysis of $B^0 \to \rhoz \KS \g$ decays. 
The large sample expected to be obtained by Belle II could open new possibilities for analysis techniques that do not depend on the Isospin symmetry hypothesis and the dilution factor.
With a large-enough data sample it could become possible to extract all the parameters from the $B^0 \to \rhoz \KS \g$ mode without any use of the charged decay mode. 
Ideally, a time-dependent amplitude analysis could be performed, including the three invariant-mass dimensions (\mKpipi, \mKpi, \mpipi). 
If this is not possible, a two-stage fit could be performed: first, a fit to the \mKpipi spectrum and then, using the results, a second fit to the \mKpi--\mpipi plane in intervals of \mKpipi.
\end{comment}

\begin{comment}
The total uncertainties on the dilution factor are largely dominated by the knowledge of the kaonic resonances and their decay rates into the various intermediate states. 
Therefore, the analysis method proposed by \babar would greatly benefit from a better knowledge of kaonic resonance properties. 
For instance, the S-wave decay of the $K_1(1270)$ resonance is poorly known, and some contradictions concerning it appear in the literature. 
{\bf [EB: Do we really need all the details in the coming part of this paragraphe?]}
Indeed, the PDG~\cite{PDG2012} quotes: $\BR (K_1(1270) \to \Kst_0(1430)\pi) = 0.28 \pm 0.04$, while the original paper from the ACCMOR collaboration~\cite{daum} that reports this measurement, refers to a strongly coupled peak in the ``scalar $+$ $\pi$'' channel around the mass $M_{K\pi\pi} \sim 1270 \mevcc$, but does not identify this scalar as the $\Kst_0(1430)$ resonance. 
In this reference, the scalar meson is parameterized with a mass of $ \simeq 1.25 \gevcc$ and a large width of $\simeq 600 \mevcc$, which can be considered as a continuum $\swave$ component.
In addition, in an analysis of $B \to \jpsi (\psi')K\pi\pi$ decays performed by the Belle collaboration~\cite{Guler:2010if}, the branching fraction of the $\Kst_0(1430)\pi$ intermediate resonant mode was measured to be  $\BR(K_1(1270) \to \Kst_0(1430)\pi) \simeq 2\%$, which is much smaller than the value given by the PDG. 
In the Belle analysis, the $\Kst_0(1430)\pi$ channel is described as a scalar resonance of mass $(1425 \pm 50) \mevcc$ and width $(270 \pm 80) \mevcc$ using a Relativistic Breit-Wigner line shape, which is different from the scalar meson parametrization used in the analysis from ACCMOR.  
Moreover, the analysis of $B^{+}\to \Kp\pip\pim\g$ decays, performed by the \babar Collaboration~\cite{Sanchez:2015pxu}, yields a large contribution of the S-wave component in the $K\pi$ invariant mass spectrum. 
In the \babar analysis, the $0^+$ component of the $K\pi$ spectrum is described by the LASS~\cite{lass} parametrization, consisting of the $\Kst_0(1430)$ resonance together with an effective range non-resonant (NR) component. 
Separating the NR part in the LASS term, and assuming the $\Kst_0(1430)$ coming only from the $K_1(1270)$ kaonic resonance, they measured $\BR( K_1(1270) \to \Kst_0(1430) \pi ) = (3.28 ^{+0.89}_{-0.98}) \times 10^{-2}$, which is in good agreement with the value measured by Belle~\cite{Guler:2010if}. 
Then, the large contribution of the NR part of the LASS term observed in the \babar analysis could be interpreted as the presence of a broad scalar resonance contributing to the $K_1(1270)$ decays, even though it was not explicitly described. 
%TODO Kappa

The \babar analysis~\cite{Sanchez:2015pxu} provided significantly improved measurements of amplitudes of the intermediate kaonic resonance states $B\to\Kres\g$. Nevertheless, there is room for much more improvements, especially concerning the $K_1(1270)$ properties, which could be achieved thanks to the large samples of \B decays recorded by LHCb and in the near future by Belle II.
\end{comment}

\begin{comment}
The time-dependent \CP-asymmetry in the 
$B^{0}\to K_{S}^{0}\rho^{0}\g$ decay can provide the information on the photon polarisation as shown in Eq.(25), {\color{red}(in the note)}, and using the Wilson coefficients $C_{7\g}$ and $C_{7\g}'$ replacing $c$ and $c'$,
\begin{equation}\label{SKsrhogamma}
\mathcal{S}_{K_{S}^{0}\rho^{0}\g} = - {2Im\left[e^{-2i\phi_{1}(\beta)}\left({C_{7\g}'\over C_{7\g}^{*}}\right)\right]\over 1+\left|{C_{7\g}'\over C_{7\g}}\right|^{2}}.
\end{equation}
Similarly, the time-dependent \CP asymmetry in the  $B^{0}\to K_{S}^{0}\pi^{0}\g$ decay is given in Eq.(29){\color{red} (in the note)} but with an opposite sign due to the \CP eigenvalues
\begin{equation}\label{SKspigamma}
\mathcal{S}_{K_{S}^{0}\pi^{0}\g}= {2Im\left[e^{-2i\phi_{1}(\beta)}\left({C_{7\g}'\over C_{7\g}^{*}}\right)\right]\over 1+\left|{C_{7\g}'\over C_{7\g}}\right|^{2}}.
\end{equation}
Note that in the above equations we have neglected the long-distance effects from $O_{2}$ operator with charm quark, which could entail a theoretical uncertainty of $2\%-10\%$ on the ratio of $\overline A_{R}^{K_{S}^{0}\rho^{0}(\pi^{0})}/\overline A_{L}^{K_{S}^{0}\rho^{0}(\pi^{0})}$ [hep-ph/0412019][hep-ph/0510104][hep-ph/9702318][hep-ph/0609037]. Considering the physical value of charm quark mass in [1206.1502], it is believed that this $O_{2}$ contribution is small. As stressed in [1206.1502], new physics effects can be established if its deviation from the SM is large.
The current world averaging result of  $\mathcal{S}_{K_{S}^{0}\pi^{0}\g}$ is [HFAG] 
\begin{equation}
\mathcal{S}_{K_{S}^{0}\pi^{0}\g}=-0.15\pm0.20.
\end{equation}
It is more precise than the recent measurement of $\mathcal{S}_{K_{S}^{0}\rho^{0}\g}$ in 
(\ref{SKsrhogammaBabar}). 

More data are required to understand 
the opposite photon polarisation, $C_{7\g}'/C_{7\g}$,  which can be obtained by the measurements of $\mathcal{S}_{K_{S}^{0}\rho^{0}\g}$ and $\mathcal{S}_{K_{S}^{0}\pi^{0}\g}$ in the Belle II experiment in the near future.
\end{comment}


\begin{comment}
As repeated in the previous sections, the measurements of the polarisation parameters require a careful separation of the resonances or the dilution factor. However, we should note that if we assume that the SM is fully left-handed, i.e. $\SKsrho=0$,  the \textit{SM null test} can be performed only with the $\SKspipi$ measurement since $\SKspipi\neq 0$ implies $\SKsrho\neq 0$. Hence the following ratio provides the \textit{statistical significance} of $\SKsrho$ deviating from zero: 
\begin{equation}
{\rm BSM\ confidence \ level}=\left|\frac{\SKspipi}{\sigma_\SKspipi}\right|
\end{equation}
Thus, a significant deviation of the measured $\SKspipi$ from zero can be directly interpreted as a discovery of new physics irrespective to the dilution factor. For example, the current central values $\SKspipi\simeq 0.1-0.2$, if confirmed, would lead to 5-10 $\sigma$ effect of new physics with the foreseen Belle II sensitivity,  $\sigma_\SKspipi \sim 0.02-0.03$. 

On the other hand, at this level of precision, we can not neglect any small SM contribution, i.e the contribution from $\ms/\mb$ as well as the so-called charm loop contribution. The charm loop contribution is estimated to be up to a few \% and this effect might be able to be measured at the future experiment. 
\end{comment}


\begin{comment}
\begin{figure}[!htbp]
\begin{center}
\includegraphics[scale=0.25]{1}
\includegraphics[scale=0.25]{2}
\includegraphics[scale=0.25]{3}
\includegraphics[scale=0.25]{4}
\includegraphics[scale=0.25]{5}
\end{center}
\caption{Figures of the standard deviations of $S_{K_{S}^{0}\rho^{0}\g}$ from zero corresponding to the values of $\mathcal{S}_{K_{S}^{0}\rho^{0}\g}$ and $\D_{K_{S}^{0}\rho^{0}\g}$. {\color{red}(May choose some of them.)} The blue line in each figure is the central value of $\mathcal{S}_{K_{S}^{0}\pip\pim\g}^{\rm Babar+Belle}=0.12\pm0.19$ combining the measurements by Babar and Belle.
}
\end{figure}

\begin{tabular}{ccccc}
\hline\hline
 & Belle[2006] & Belle[2008] & Belle II (10\ab) & Belle II (50\ab)
 \\
 \hline
 Luminosity & $\sim500$\fb$ & $\sim700$\fb$ & 10\ab & 50\ab
 \\
$\mathcal{S}_{K_{S}^{0}\pip\pim\g}$ & $-$ & $0.09\pm0.27^{+0.04}_{-0.07}$ & $\pm0.07$ & $\pm0.03$
\\
$\D_{K_{S}^{0}\rho^{0}\g}$ & $-$ & $0.83^{+0.19}_{-0.03}$ & $\pm0.05$ & $\pm0.02$
\\
$\mathcal{S}_{K_{S}^{0}\rho^{0}\g}$ & $-$ & $0.11\pm0.33^{+0.05}_{-0.09}$ & $\pm0.09$ & $\pm0.04$
\\
$\mathcal{S}_{K_{S}^{0}\pi^{0}\g}$ &$-0.10\pm0.31\pm0.07$  & $-$& $\pm0.07$ & $\pm0.03$
\\
\hline
\end{tabular}

\end{comment}
