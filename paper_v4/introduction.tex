\section{Introduction}
\label{sec:introduction}

The exclusive \btosg process is one of the most sensitive observables to new physics in \B physics: unlike many other \B-hadron decays, it is described by a single operator, the electro-magnetic type \mbox{$\squarkbar \sigma_{\mu\nu} (1+\g_5) \bquark F^{\mu\nu}$},  which minimizes the uncertainties from hadronic effects. 
In the era of the LHC and the upgraded \B factory experiment, Belle II, an interesting opportunity opens to investigate the circular-polarisation of the photon in \btosg process and gain additional insight on its nature. 
In the standard model (SM), the photon polarisation of \btosg is predicted to be predominantly left-handed due to the operator mentioned above.
On the other hand, several new-physics  models contain new particles that couple differently from the SM, namely, inducing an opposite chirality operator \mbox{$\squarkbar \sigma_{\mu\nu} (1-\g_5) \bquark F^{\mu\nu}$}; these models predict an enhanced right-handed photon contribution. Examples of such new physics models are given in Refs.~\cite{Becirevic:2012dx,Kou:2013gna,Haba:2015gwa,Paul:2016urs}. 
The photon polarisation in \btosg transitions is therefore a fundamental property of the SM, and its determination may provide information on physics beyond the SM.

The experimental measurement of the photon polarisation has been a real challenge in \B physics, and much effort has been put into it in recent years.
Mainly, two ways to measure the photon polarisation have been proposed and carried out: measuring the angular distribution of the recoiled particles (see~\cite{Gronau:2001ng,Gronau:2002rz,Kou:2010kn, Kou:2016iau,Bishara:2015yta,Oliver:2010im} for theoretical proposals and~\cite{Aaij:2015dea,Aaij:2014wgo} for experimental results), and measuring the time-dependent \CP asymmetry (Refs.~\cite{Atwood:1997zr,Atwood:2004jj,Atwood:2007qh, Muheim:2008vu} and~\cite{Aubert:2008gy,Ushiroda:2006fi, Li:2008qma,Sanchez:2015pxu} for theory and experiment, respectively). 
In this article, we discuss the second method.

Extracting information on the photon polarisation from the time-dependent \CP asymmetry measurement is illustrated with the promising channel \mbox{$\B \to \Kres \g \to (n\pi)\KS\g$}, where \Kres is a kaonic resonance and $n\pi$ designates $n$ pions that form a \CP eigenstate. The illustration is depicted in Fig~\ref{fig:1}.
%The way to extract information on the photon polarisation from the  the time-dependent \CP asymmetry  measurement is schematically depicted in Fig.~\ref{fig:1}, using the promising channel $B\to \Kres\g \to (n\pi)\KS\g$, where \Kres is a kaonic resonance and $(n\pi)$ designates $n$ pions that form a \CP eigenstate. 
The time-dependent \CP asymmetry originates from the interference of the processes \mbox{$\B \to \Kres \g \to (n\pi) \KS \g$} and \mbox{$\Bb \to \Kresbar\g \to (n\pi)\KS\g$}, one of which appears from a $\B-\Bb$ oscillation. The important point is that interfere occurs only when photons coming from \B and \Bb are circularly polarized in the same direction. 
Let us define the rate of  \Bb (\B) mesons decaying into left- (right-) handed photons to be \Wc, and the rate of \Bb (\B) into right- (left-) handed photons  to be \cprime. The former process is induced by the standard operator contribution \mbox{$\squarkbar \sigma_{\mu\nu} (1+\g_5) \bquark F^{\mu\nu}$}, and the latter is induced by the non-standard operator \mbox{$\squarkbar \sigma_{\mu\nu} (1-\g_5) \bquark F^{\mu\nu}$}.
%Let us define the rate of  \Bb (\B) mesons decaying into left- (right-) handed photons (induced by the standard operator contribution \mbox{$\squarkbar \sigma_{\mu\nu} (1+\g_5) \bquark F^{\mu\nu}$}) to be \Wc, and the rate of \Bb (\B) into right- (left-) handed photons (induced by the non-standard operator \mbox{$\squarkbar \sigma_{\mu\nu} (1-\g_5) \bquark F^{\mu\nu}$}) to be \cprime. 
Knowing that in the SM \mbox{$\cprime/\Wc = \ms/\mb (\simeq 0)$}, that is, the left- (right-) handed photon is nearly forbidden for a \B (\Bb) meson decay, the interference of \B and \Bb would
%not occur.
be nearly zero. Therefore, an observation of the non-zero \CP asymmetry can be considered to be the immediate signal of new physics. Once non-zero \CP asymmetry is observed, one can further determine the ``photon polarisation'', which means to determine the ratio of $\cprime/\Wc$ by using the oscillation parameters ($q/p$ in Fig.~\ref{fig:1}) obtained from the $\sin 2\beta$ measurement in other modes, such as \mbox{$\Bz \to \jpsi \KS$}.

The simplest channel to study in this regard is  \mbox{$\Bz \to \Kstarz \g \to \KS \piz \g$}, for which the first
measurements of the mixing induced \CP violation have been
obtained by the \babar~\cite{Aubert:2008gy} and Belle~\cite{Ushiroda:2006fi} experiments:
\mbox{$S_{\KS\piz \g}^{\scriptsize \babar}=-0.03\pm 0.29\pm 0.03$} and
\mbox{$S_{\KS\piz \g}^{\rm Belle}=-0.32^{+0.36}_{-0.33}\pm 0.05$}. 
As the statistical uncertainties dominate in these measurements, they could be largely improved by the Belle II experiment, where it is planed to accumulate in the coming ten years 70 to 100 times more data than in the first-generation \B factories.


In this paper, we discuss an alternative method that can
 provide a measurement of the mixing induced \CP violation in the decay \mbox{$\Bz \to \Kres \g \to \rhoz \KS \g \to \pip \pim \KS \g$}. 
Investigating this channel allows
firstly to confirm the other measurements, and secondly to increase the experimental sensitivity, especially in the early time of the Belle II experiment. 

The main difficulty comes from the fact that the final state \mbox{$\pip \pim \KS$} can originate not only from the \CP eigenstate \rhoKs but also from other intermediate states. 
The determination of the \CP asymmetry of the \rhoKs channel is disturbed in particular by non \CP eigenstates, such as $K^{*\pm} \pi^\mp$.
%In particular, non \CP eigenstates, such as $K^{*\pm}\pi^\mp$, disturb the determination of the \CP asymmetry of the $\rhoz \KS$ channel.
In order to disentangle these contributions,
a detailed amplitude analysis is required. Such analysis
 has been pioneered by the Belle collaboration~\cite{Li:2008qma} and extended by the \babar collaboration~\cite{Akar:2013ima}.

\begin{figure}[htb]
%\hspace*{-0.8cm}\begin{minipage}{8.8cm}
%\hspace*{-0.6cm}\begin{minipage}{8.5cm}
{\relsize{-1.3}
\begin{minipage}{0.48\textwidth}
\begin{eqnarray}
 & \Bz \ {{\cprime(\Wc)}  \atop \longrightarrow}\ \Kres \g_{L (R)}  & \nonumber \\
\phantom{B(0)}^{ f_+(t)}\nearrow& \hspace*{2.cm}& \searrow \nonumber \\
\Bz(t=0)\hspace*{0.5cm}&\hspace*{2.cm}&  (n\pi) \KS \g_{L (R)} \; \nonumber \\
\phantom{B(0)}_{ \qoverp f_-(t)}\searrow & \hspace*{2.cm}& \nearrow\nonumber \\
& \Bzb \ {{\Wc(\cprime)}  \atop \longrightarrow}\ \Kresbar \g_{L (R)} & \nonumber \end{eqnarray}
\end{minipage}
%%%
%\begin{minipage}{8.5cm}
\hspace*{0.3cm}\begin{minipage}{0.48\textwidth}
\begin{eqnarray}
 & \Bz\ {{\cprime(\Wc)}  \atop \longrightarrow}\ \Kres \g_{L (R)}  & \nonumber \\
\phantom{B(0)}^{\frac{p}{q}f_-(t)}\nearrow& \hspace*{2.cm}& \searrow\nonumber \\
\Bzb(t=0)\hspace*{0.5cm}&\hspace*{2.cm}&   (n\pi) \KS \g_{L (R)} 
\nonumber \\ 
\phantom{B(0)}_{ f_+(t) }\searrow & \hspace*{2.cm}& \nearrow \nonumber \\
& \Bzb \ {{\Wc(\cprime)}  \atop \longrightarrow}\ \Kresbar \g_{L (R)} & \nonumber 
\end{eqnarray}
\end{minipage}
}
\caption{Schematic description of the time-dependent \CP asymmetry of \mbox{$B\to \Kres \g \to (n\pi)\KS \g$}.
The factors $f_-(t)$ and $f_+(t)$ are the time-dependent oscillation and non-oscillation probability, respectively, of a $\Bz$ or a $\Bzb$ meson.
The $q$ and $p$ are the $\B-\Bb$ oscillation parameters, which correspond to  \mbox{$q/p =(\Vtbstar\Vtd)/(\Vtb\Vtdstar)=e^{-2i\beta}$} in the SM.
The coefficients \Wc and \cprime represent the ratio of the the standard operator contribution \mbox{$\squarkbar \sigma_{\mu\nu} (1+\g_5) \bquark F^{\mu\nu}$} and that of the non-standard one \mbox{$\squarkbar \sigma_{\mu\nu} (1-\g_5) \bquark F^{\mu\nu}$}, respectively. 
In the SM, $\cprime/\Wc=\ms/\mb (\simeq 0)$, and therefore one of the passes is forbidden, the interference would not occur, and the \CP asymmetry would be zero.}
\label{fig:1}
 \end{figure}



In this paper, motivated by these developments, we re-visit the method to obtain the mixing-induced \CP asymmetry for \nChannelRhoz in \mbox{$\Bz \to \Kres \g \to \pip \pim \KS \g$} decays
% that provides further
to gain more insight on the photon polarisation.
One indispensable ingredient of the method is the \CP-eigenvalue, or ``\CP-sign'', that intervenes in the measurement, considering the dominant intermediate decay channels: 
\begin{eqnarray}
K_1(1270), \quad K_1(1400) && (J^P=1^+), \nonumber \\
\Kst(1410), \quad \Kst(1680) && (J^P=1^-), \nonumber \\
K_2^*(1430) && (J^P=2^+), \nonumber 
\end{eqnarray}
for the kaonic resonances and 
 \begin{eqnarray}
& \Bz \to \Kres \g \to (\rhoKs)   \g \to \KS(\pip\pim)\g,   \nonumber \\
& \Bz \to \Kres \g \to (\KstPi)   \g \to (\KS\pip)\pim\g, & \nonumber \\
& \Bz \to \Kres \g \to (\KappaPi) \g \to (\KS\pip)\pim\g, & \nonumber 
\end{eqnarray}
for the $K\pi$ or $\pi\pi$ intermediate states. The notation \swave designates the $K\pi$ $S$-wave.
A derivation of this \CP-sign has not been demonstrated before.
Another ingredient of the method is the ``dilution factor'' that allows to obtain the specific value of the mixing induced \CP asymmetry for \nChannelRhoz from the raw measurement of this quantity in \nChannelPiPi decays.



In \secref{sec:overview} we introduce the time dependent \CP asymmetry formulae for \mbox{$\Bz \to \Kres \g \to \pip \pim \KS \g$} decays and the dilution factor. 
In \secref{sec:amplitudes} we derive the \CP-sign for the decay amplitudes with different intermediate states, which is required in order to extract the \CP asymmetry.
Using this result we derive the dilution factor in \secref{sec:dilutionFactor},
and in \secref{sec:application} we describe the way to extract it from \mbox{$\Bp \to \Kresp \g \to \Kp \pim\pip\g$} decays.\footnote{Charge conjugation is implicit throughout the document.}
Finally, in \secref{sec:interpretation} we discuss the future prospects for the measurement of the \CP asymmetry at Belle II and for that of the dilution factor at LHCb. We then conclude in \secref{sec:conclusion}.















