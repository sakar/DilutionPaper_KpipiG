----------------------------------------------------------------
## Getting the source code
```
git clone https://sakar@gitlab.cern.ch/sakar/DilutionPaper_KpipiG.git
```

## Documentation

The latest paper tex sources are in the repository: `paper_v4/`, while the notes describing the details of the differents steps are documented in the repository `notes/`.

### Notes

The note `Eli.pdf` describes the part of the computation related to the Kres decay part in the paper (i.e. Sec.3.2),
while the note `Simon_tmp.pdf` goes through the whole computation until the numerical application to experimental results (i.e. Sec.5).
Many useful detailled parts of the computation used in various places are described in the note `Simon_tips_tmp.pdf`.